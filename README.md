Public repo with some simple k8s yaml files

# Ubuntu Minikube configuration

PS: I did this in a fresh installation on ubuntu in a VM

Install kubectl
```
apt update
apt install git curl
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s \
	https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

install docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/ 

Install Minikube itself
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
sudo mv minikube /usr/local/bin/minikube
sudo minikube start --vm-driver=none
sudo minikube status
sudo kubectl get nodes
```

You will need this minikube command in the future `sudo minikube ip`

References:

https://kubernetes.io/docs/tasks/tools/install-minikube/

https://kubernetes.io/docs/setup/learning-environment/minikube/#specifying-the-vm-driver

https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux

# macOS

If you are using macOS

- Install docker-desktop
- go to "Preferences > Kubernetes"
- click "enable kubernetes"
- exit preferences
- wait 5 min for kubernetes to be available
- click the docker-desktop icon
- go to "Kubernetes" and select the docker-desktop context
